<?php
 

 $connect = mysqli_connect("65.211.74.16:3306", "reportreader", "5baRnpatRNUjLmVH3AcT", "ssctest");

//error
if (!$connect)
	
	{
	echo"Connection Fail". mysqli_connect_errno();	

	}
	else 
	{
		//echo"connection sucessful trying to exucute the query";
	}
//query
$sql="select pro.shop as woshops,phs.shop as phaseshops,pro.proposal,phs.sort_code,phs.status_code as phasestatus, pro.status_code as wostatus,
phs.ent_date as phs_entdate,datediff(curdate(),phs.ent_date) as phs_duration,
phs.pri_code,pro.wo_pri_code,
case when phs.craft_code in ('FIRE ALARM RESPONSE','FIRE/SUPP SYS') then 'FLS' 
when phs.craft_code in ('HVAC', 'BOILERS', 'CHILLERS', 'FILTERS') then 'HVAC'
when phs.craft_code in ('ROOFING') then 'Roofs'
when phs.craft_code in ('CHW', 'HHW', 'DCW', 'DHW', 'COMPRESSORS', 'ELECTRICAL', 'ELEVATORS', 'PLUMBING', 'PUMPS','WATER PURIFICATION') then 'MEP'
when phs.craft_code in ('CARPENTRY', 'CUSTODIAL', 'FURNITURE REPAIR', 'GENERAL MAINT', 'GRAPHICS', 'LIGHTS', 'PAINTING') then 'Interiors'
when phs.craft_code in ('CONCRETE REPAIR') then 'Grounds' else phs.craft_code end as carftcode
,pro.bldg
 from ae_p_pro_e pro
 left outer join ae_p_phs_e phs on phs.proposal = pro.proposal
where pro.category in ('CM','SR') 
and pro.order_type='M' 
and pro.status_code in ('PENDING BUDGET APP','REQ TAMU APPROVAL','WORK STARTED','OPEN')
/*and phs.pri_code  in ('4')*/
/*and phs.proposal is not null*/
and phs.status_code not in ('PHS COMPLETE','CONTRACTOR COMPLETE','COMPLETE','PHS CANCEL','CLOSED')
and pro.shop not in ('REC SPORTS - GROUNDS','RELLIS - GROUNDS','GROUNDS SPECIAL PROJECTS','STRUCTURAL PEST','HORT','IRRIGATION',
'CUSTODIAL','GROUNDS','HEQ','LCON','SANITATION','TREE','TURF','EDCS','EAST CAMPUS HORT','EAST CAMPUS TURF','L&O','MECHANICS','IPM')
group by phs.proposal,phs.sort_code
order by phs.proposal DESC";
//execute sql
$result= mysqli_query($connect,$sql);
if (!$result)
{
	echo "error select statement".mysqli_error($result)."<br>";
}
else
{
	//echo "<br>";
	 //echo "<br>"."Number of rows returned \n".mysqli_num_rows($result)."<br>";
}
$craftcode = array(
	array("FLS"=>0),
	array("HVAC"=>0),
	array("Roofs"=>0),
	array("MEP"=>0),
	array("Interiors"=>0),
	array("Grounds"=>0));
foreach($result as $data)
{
	if($data['carftcode']=='FLS')//total
{
	$craftcode['0']['FLS']++;
}
if($data['carftcode']=='HVAC')//11-30
{                                                    
	    $craftcode['1']['HVAC']++;                                           
}                                                    
if($data['carftcode']=='Roofs')//31-60
{                                                    
	    $craftcode['2']['Roofs']++ ;                                          
}                                                    
if($data['carftcode']=='MEP')//61-90
{                                                    
	       $craftcode['3']['MEP']++ ;                                        
}                                                    
if($data['carftcode']=='Interiors')//91-180
{                                                  
	        $craftcode['4']['Interiors']++ ;                                   
}                                                  
if($data['carftcode']=='Grounds' )//+181
{
	$craftcode['5']['Grounds']++;
}
}//firstforeach
$jsondata=array();
$dataPoints = array();
 foreach($craftcode as $chart)
 {
	 foreach($chart as $x =>$y)
	 {
		 $dataPoints = array("lable"=>$x, "symbol" => $x,"y"=>$y);
		 array_push($jsondata,$dataPoints);
	 }
 }
//print_r($craftcode);
?>

<!DOCTYPE HTML>
<html>
<head>
<script>
window.onload = function() {
 
var chart = new CanvasJS.Chart("chartContainer", {
	theme: "light2",
	animationEnabled: true,
	title: {
		text: "Work Order by CraftCode"
	},
	data: [{
		type: "doughnut",
		indexLabel: "{symbol} - {y}",
		yValueFormatString: "#,###",
		showInLegend: true,
		legendText: "{symbol}:{y}{label}",
		dataPoints: <?php echo json_encode($jsondata, JSON_NUMERIC_CHECK); ?>
	}]
});
chart.render();
 
}
</script>
</head>
<body>
<div id="chartContainer" style="height: 370px; width: 100%;"></div>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
</body>
</html>                              