<!DOCTYPE html >
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <title>Using MySQL and PHP with Google Maps</title>
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>
  </head>

<html>
  <body>
    <div id="map"></div>

    <script>
      
function initMap() {
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
        mapTypeId: 'roadmap'
    };
                    
    // Display a map on the web page
    map = new google.maps.Map(document.getElementById("mapCanvas"), mapOptions);
    map.setTilt(50);
        
    // Multiple markers location, latitude, and longitude
    var markers = [
        <?php 
		$connect = mysqli_connect("65.211.74.16:3306", "reportreader", "5baRnpatRNUjLmVH3AcT", "ssctest");

//error

$query = "select pro.bldg,bldg.latitude,bldg.longitude
 from ae_p_pro_e pro
 left outer join ae_p_phs_e phs on phs.proposal = pro.proposal
 join ae_s_bld_c bldg on pro.bldg =bldg.bldg
where pro.category in ('CM','SR') 
and pro.order_type='M' 
and pro.status_code in ('PENDING BUDGET APP','REQ TAMU APPROVAL','WORK STARTED','OPEN')
and bldg.latitude is not null
and bldg.longitude is not null
/*and phs.pri_code  in ('4')*/
/*and phs.proposal is not BILLABLE
and phs.status_code not in ('PHS COMPLETE','CONTRACTOR COMPLETE','COMPLETE')*/
and pro.shop not in ('REC SPORTS - GROUNDS','RELLIS - GROUNDS','GROUNDS SPECIAL PROJECTS','STRUCTURAL PEST','HORT','IRRIGATION',
'CUSTODIAL','GROUNDS','HEQ','LCON','SANITATION','TREE','TURF','EDCS','EAST CAMPUS HORT','EAST CAMPUS TURF','L&O','MECHANICS','IPM')
group by phs.proposal";
$result= mysqli_query($connect,$query);
		if($result->num_rows > 0){
            foreach($result as $rows){
                echo '["'.$rows['bldg'].'", '.$rows['latitude'].', '.$rows['longitude'].'],';
            }
        }
        ?>
    ];
                        
    // Info window content
    
        
    // Add multiple markers to map
    var infoWindow = new google.maps.InfoWindow(), marker, i;
    
    // Place each marker on the map  
    for( i = 0; i < markers.length; i++ ) {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            title: markers[i][0]
        });
        
        // Add info window to marker    
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            }
        })(marker, i));

        // Center the map to fit all markers on the screen
        map.fitBounds(bounds);
    }

    // Set zoom level
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(14);
        google.maps.event.removeListener(boundsListener);
    });
    
}

// Load initialize function
//google.maps.event.addDomListener(window, 'load', initMap);
</script>
  
    <script defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCd0NP9HuwC5TsarIgrkjLXzK5J4pO5ek4">
    </script>
  </body>
</html>