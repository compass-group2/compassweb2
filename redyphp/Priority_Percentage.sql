select 
 table1.entrydate, table1.created, table1.Priority_1, table1.Priority_2, table1.Priority_3, table2.NO_OF_priority1_ON_TIME, table2.NO_OF_priority1_NOT_ON_TIME, table2.NO_OF_priority2_ON_TIME,table2. NO_OF_priority2_NOT_ON_TIME, table2.NO_OF_priority3_ON_TIME, table2.NO_OF_priority3_NOT_ON_TIME
 ,truncate((((table2.NO_OF_priority1_ON_TIME))/(table1.Priority_1))*100,2) as PR1_ON_Timeprecent,
truncate((((table2.NO_OF_priority2_ON_TIME))/(table1.Priority_2))*100,2) as PR2_ON_Timeprecent,
truncate((((table2.NO_OF_priority3_ON_TIME))/(table1.Priority_3))*100,2) as PR3_ON_Timeprecent
from 

(select entrydate,((sum(PR1))+(SUM(PR2))+(SUM(PR3))) as created,sum(PR1) as Priority_1,SUM(PR2) as Priority_2,SUM(PR3) as Priority_3 from

(
select count(wo) as created,date_format(entrydate,"%Y,%m") as entrydate,
case when  priority ='1' then count(a.wo) else 0 end as PR1,
case when  priority ='2' then count(a.wo) else 0  end as PR2,
case when  priority ='3' then count(a.wo) else 0  end as PR3
from 
(
select pro.ent_date as entrydate,phs.proposal as wo,
case when phs.sort_code ='001' and phs.pri_code in ('1','1 -WITHIN 1 DAY') then 1 
when phs.sort_code ='001' and phs.pri_code in ('2','2 - 2 - 5 DAYS','2-SAME DAY') then 2 
when phs.sort_code ='001' and phs.pri_code in ('3','3-ROUTINE','3 - 5 - 10 DAYS') then 3 
when phs.sort_code ='001' and phs.pri_code in ('4','4-SCHEDULED') then 4 
when phs.sort_code ='001' and phs.pri_code in ('PM1','PM2','PM3') then 'PM' else null end as priority,
 CONCAT(phs.shop,'') as PhaseShop, CONCAT(pro.shop,'') as WorkOrdershop,pro.category,pro.order_type
from  ae_p_pro_e pro
join  ae_p_phs_e phs on phs.proposal = pro.proposal 
where  
 phs.status_code not in ('DUPLICATE','PHS CANCEL','CANCELED')
  and pro.status_code not in ('CANCELED','DUPLICATE')
  and phs.proposal is not null
and phs.pri_code not in ('4','4-SCHEDULED','PM1','PM2','PM3') 
and date(pro.ent_date) between  '2020-01-01' and '2020-06-01'
and pro.order_type ='M'
/*phs.shop in ()*/
/*and hist.proposal ='170907-304222'*/
group by pro.proposal
order by pro.proposal desc
)a
group by entrydate,priority) y 
group by y.entrydate ) 
as table1
join
(
select b.monthnumber,
count(b.OnTime1) as NO_OF_priority1_ON_TIME ,
count(b.NotOnTime_1) as NO_OF_priority1_NOT_ON_TIME ,
count(b.OnTime2) as NO_OF_priority2_ON_TIME,
count(b.NotOnTime_2)as NO_OF_priority2_NOT_ON_TIME ,
count(b.OnTime3) as NO_OF_priority3_ON_TIME,
count(b.NotOnTime_3) as NO_OF_priority3_NOT_ON_TIME from 
(select case when x.phs_age between '0' and '1'  and priority ='1' then count(x.wo)  end as OnTime1, case when x.phs_age>'1'  and priority ='1' then count(x.wo)  end as NotOnTime_1, 
case when x.phs_age between '0' and '5'   and priority ='2' then count(x.wo) end as OnTime2,case when x.phs_age > '5'  and priority ='2' then count(x.wo) end as NotOnTime_2,
case when x.phs_age between '0' and '10'   and priority ='3' then count(x.wo) end as OnTime3 ,case when x.phs_age > '10' and priority ='3' then count(x.wo)  end as NotOnTime_3 ,
date_format(x.completeddate,"%Y,%m") as monthnumber from
(
select hist.proposal as wo, CONCAT(hist.status_code,'') as histstatus, 
CONCAT(phs.status_code,'') as currentphasesatus,pro.ent_date as entrydate,
max(hist.status_date) as completeddate,
case when phs.pri_code in ('1','1 -WITHIN 1 DAY') then 1 
when phs.pri_code in ('2','2 - 2 - 5 DAYS','2-SAME DAY') then 2 
when phs.pri_code in ('3','3-ROUTINE','3 - 5 - 10 DAYS') then 3 
when phs.pri_code in ('4','4-SCHEDULED') then 4 
when phs.pri_code in ('PM1','PM2','PM3') then 'PM' else null end as priority,
 CONCAT(phs.shop,'') as PhaseShop, CONCAT(pro.shop,'') as WorkOrdershop,pro.category,pro.order_type,
 datediff( max(hist.status_date), pro.ent_date) as phs_age
from ae_p_pro_e pro 
join ae_p_phs_e phs on phs.proposal = pro.proposal 
join ae_p_sta_e hist on pro.proposal = hist.proposal 
where  (hist.status_code) in 
('WO COMPLETE','TRANSFERRED TO UES','UES RECOMND','TRANSFERRED TO BA','workorder complete')
and phs.pri_code not in ('4','4-SCHEDULED','PM1','PM2','PM3')
 and pro.status_code not in ('CANCELED','DUPLICATE')
and date(pro.ent_date) between  '2020-01-01' and '2020-06-01'
and pro.order_type ='M'
/*phs.shop in ()*/
/*and hist.proposal ='170907-304222'*/
group by pro.proposal
)x
group by x.completeddate
) b
group by b.monthnumber 
) as table2
on table2.monthnumber = table1.entrydate
