select 
table1.entgivenmonth as yearmonth,table1.entry, table1.billable, table1.sscpay, table1.nobillabletag, table1.nonotes, table1.pr1, table1.pr2, table1.pr3,table1.pr4, table1.PM,
table2.compsamemonth, table2.notsamemonth,
truncate(((table2.compsamemonth+table2.notsamemonth)/table1.entry)*100,2) as overall_completepercent_Bymonth,
truncate((table2.compsamemonth/table1.entry)*100,2) as completepercent_complete_samemonth
from 
(select count(a.created) as entry,sum(billable) as billable ,sum(sscpay) as sscpay,sum(nobillabletag) as nobillabletag,sum(nonotes) as nonotes,
sum(pr1) as pr1,sum(pr2) as pr2,sum(pr3) as pr3,sum(pr4) as pr4,sum(PM) as PM,
a.entdate as entgivenmonth from 
(select date_format(pro.ent_date,"%Y,%m") as entdate ,(pro.proposal) as created,pro.wo_pri_code as billablestatus,
case when pro.wo_pri_code in ('BILLABLE','PM5K','DEFERRED') then 1 else 0 end as billable,
case when pro.wo_pri_code in ('NON BILLABLE','SSC PAY') then 1 else 0 end as sscpay,
case when pro.wo_pri_code is null then 1 else 0 end as nobillabletag,
case when notes.notes is null/*and notes1.notes*/ is null then 1 else 0 end as nonotes,
case when phs.sort_code ='001' and phs.pri_code in ('1','1 -WITHIN 1 DAY') then 1 else 0 end as pr1,
case when phs.sort_code ='001' and phs.pri_code in ('2','2 - 2 - 5 DAYS','2-SAME DAY') then 1 else 0 end as pr2,
case when phs.sort_code ='001' and phs.pri_code in ('3','3-ROUTINE','3 - 5 - 10 DAYS') then 1 else 0 end as pr3,
case when phs.sort_code ='001' and phs.pri_code in ('4','4-SCHEDULED') then 1 else 0 end as pr4,
case when phs.sort_code ='001' and phs.pri_code in  ('PM1','PM2','PM3')  then 1 else 0 end as PM
 from  ae_p_pro_e pro 
   join ae_p_phs_e phs on phs.proposal =pro.proposal
left outer  join ae_p_phs_e_notes notes on  pro.proposal = notes.proposal and phs.proposal =notes.proposal 
 /*left outer join ae_p_pro_e_notes notes1 on notes1.proposal =pro.proposal*/
 where date(pro.ent_date) between  '2020-01-01' and '2020-06-01'
 and pro.order_type ='M'
 and phs.status_code not in ('DUPLICATE','PHS CANCEL','CANCELED')
 and pro.status_code not in ('CANCELED','DUPLICATE')
 and phs.proposal is not null
 group by pro.proposal
 )a group by a.entdate 
 )as table1
 join
  (
select sum(compinsamemonth) as compsamemonth,sum(notinsamemonth) as notsamemonth,x.months 
from (
select case when date_format(entrydate,"%Y,%m") = date_format(completeddate,"%Y,%m")then 1 else 0 end as compinsamemonth,
case when date_format(entrydate,"%Y,%m") != date_format(completeddate,"%Y,%m")then 1 else 0 end as notinsamemonth,
date_format(c.entrydate,"%Y,%m") as months,c.* from 
 (
 select date(pho.ent_date) as entrydate,date(max(hist.status_date)) as completeddate,hist.proposal from 
ae_p_sta_e hist
 left outer join ae_p_pro_e pho on hist.proposal =pho.proposal
where hist.status_code in ('WO COMPLETE','TRANSFERRED TO UES','UES RECOMND','TRANSFERRED TO BA')
and pho.status_code not in ('CANCELED','DUPLICATE')
and date(pho.ent_date) between '2020-01-01' and '2020-06-01'
/*and date(hist.status_date) between ? and ?*/
 and pho.order_type ='M'
group by hist.proposal
)c 
)x 
group by x.months
) as table2
on table1.entgivenmonth =table2.months
