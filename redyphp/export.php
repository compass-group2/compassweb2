<?php  
//export.php  
$connect = mysqli_connect("65.211.74.16:3306", "reportreader", "5baRnpatRNUjLmVH3AcT", "ssctest");
$output = '';
if(isset($_POST["export"]))
{
 $query = "select a.wo,a.sortcode,a.category,case when a.time_type ='OVERTIME' then sum(a.act_hrs)
 when a.time_type ='STRAIGHT TIME' then sum(a.act_hrs) else a.act_hrs end as hrs,
 a.time_type,
 case when a.time_type ='OVERTIME' then sum(a.act_cost)
when a.time_type ='STRAIGHT TIME' then sum(a.act_cost) else a.act_hrs end as labor_cost
,a.wo_pri_code as billablestatus,a.asset_tag,a.shop,a.ent_date as entrydate,a.status_code
 
   from
   (
select det.proposal as wo,det.time_type,phs.asset_tag,phs.shop,phs.ent_date,phs.status_code,pro.category,
det.act_hrs ,phscost.proposal as porposal,phscost.sort_code as sortcode,det.act_cost,
phscost.ACTUAL_HOURS,phscost.ACTUAL_LABOR,phscost.BILLED_LABOR,phscost.BILLED_TOTAL,phscost.ACTUAL_TOTAL, pro.wo_pri_code from
ae_p_phs_e phs
join ae_p_wka_d det on phs.proposal =det.proposal and phs.sort_code =det.sort_code
join aim_phase_cost phscost on phscost.proposal = phs.proposal and  phscost.sort_code = phs.sort_code
join ae_p_pro_e pro on phs.proposal =pro.proposal
where date(phs.ent_date) between '2020-01-01' and '2020-01-30'
and pro.category not in ('ST')
and pro.order_type ='M'
and phs.shop in('OPS PROJECTS','HVAC','COMM','NIGHT','HSC','ELEC','FSW','PLUB','GRAPHICS','IMS','AMPT','MAINTENANCE','ZONE D','AWC','ELEVATOR','GBL',
'ZONE A','ZONE C','ZONE B','ZONE R NORTH','REC SPORTS','ATHLETICS','UCC','TRADES','FAS','ZONE R SOUTH','TRADES')
)a
group by a.time_type,a.porposal,a.sortcode";
 $result = mysqli_query($connect, $query);
 if(mysqli_num_rows($result) > 0)
 {
  $output .= '
   <table class="table" bordered="1">  
<tr>  
         <th>WO</th>  
         <th>SORT CODE</th>  
         <th>CATEGORY</th>  
         <th>HRS Code</th>
		 <th>TIME TYPE</th>
		 <th>LABOR COST</th>  
         <th>BILLABLE STATUS</th>  
         <th>ASSET TAG</th>  
         <th>SHOP</th>
         <th>ENTRY DATE</th>
	     <th>STATUS_CODE</th>
</tr>
  ';
  while($row = mysqli_fetch_array($result))
  {
   $output .= '
    <tr>  
         <td>'.$row["wo"].'</td>  
         <td>'.$row["sortcode"].'</td>  
         <td>'.$row["category"].'</td>  
         <td>'.$row["hrs"].'</td>  
         <td>'.$row["time_type"].'</td>
		 <td>'.$row["labor_cost"].'</td>  
         <td>'.$row["billablestatus"].'</td>  
         <td>'.$row["asset_tag"].'</td>  
         <td>'.$row["shop"].'</td>  
         <td>'.$row["entrydate"].'</td>
		 <td>'.$row["status_code"].'</td>
       </tr>  
   ';
  }
  $output .= '</table>';
  header('Content-Type: application/csv');
  header('Content-Disposition: attachment; filename=download.csv');
  echo $output;
 }
}
$connect->close();
?>
