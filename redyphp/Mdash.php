<!DOCTYPE html>
<html>
<title>SSC ANALYTICS</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
html,body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
</style>
<head>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $(".btn1").click(function(){
    $("div").hide();
  });
  $(".btn2").click(function(){
    $("div").show();
  });
});
</script>

</head>
<body class="w3-light-grey">
<!-- !PAGE CONTENT! -->
<div class="w3-main" style="margin-left:100px;margin-right:100px;"><!-- !layoutchanged! -->
 <!-- Header -->
  <header class="w3-container" style="padding-top:22px;text-align: center;">
    <h5><b><i class="fa fa-dashboard"></i> SSC MAINTAINANCE OVERALL ANALYTICS</b></h5>
  </header> <!-- PAGE CONTENT DESCP(Header)! -->
  <!-- PAGE CONTENT DIVISION! -->
  <div class="w3-row-padding">
    <div class="w3-quarter">
      <div class="w3-container w3-red w3-padding-16">
        <div class="w3-left"><i class="fa fa-gear fa-spin w3-xxxlarge"></i></div>
        <div class="w3-right">
          <h3>52</h3>
        </div>
        <div class="w3-clear"></div>
        <h4>Total Aging </h4>
		<button class="btn1">Hide</button>
		<button class="btn2">Show</button>
      </div>
    </div>
    <div class="w3-quarter">
      <div class="w3-container w3-blue w3-padding-16">
        <div class="w3-left"><i class="fa fa-tachometer fa-spin w3-xxxlarge" style="font-size:48px;color:red"></i></div>
        <div class="w3-right">
          <h3>99</h3>
        </div>
        <div class="w3-clear"></div>
        <h4>PRIORITY%</h4>
		<button class="btn1">Hide</button>
		<button class="btn2">Show</button>
      </div>
    </div>
    <div class="w3-quarter">
      <div class="w3-container w3-teal w3-padding-16">
        <div class="w3-left"><i class="fa fa-share-alt w3-xxxlarge"></i></div>
        <div class="w3-right">
          <h3>23</h3>
        </div>
        <div class="w3-clear"></div>
        <h4>PM</h4>
		<button onclick="TwoFunction()">VIEW DETAIL</button>
      </div>
    </div>
    <div class="w3-quarter">
      <div class="w3-container w3-orange w3-text-white w3-padding-16">
        <div class="w3-left"><i class="fa fa-users w3-xxxlarge"></i></div>
        <div class="w3-right">
          <h3>50</h3>
        </div>
        <div class="w3-clear"></div>
        <h4>ACCOUNTS</h4>
		<button onclick="myFunction()">VIEW DETAIL</button>
      </div>
    </div>
</div>  <!-- PAGE CONTENT DIVISION COMPLETE! -->

<div id="agingdiv" class="w3-container w3-animate-zoom">
<div class="w3-display-container" style="height:400px;">
<div class="w3-half w3-light-green">
	<iframe src="https://aggieworksdev.tamu.edu/reddy/redyphp/aginingdash.php" style="border:none;width:100%;height:400px"></iframe>
 </div>
<div class="w3-half w3-light-green">
	<iframe src="https://aggieworksdev.tamu.edu/reddy/redyphp/craftcode.php" style="border:none;width:100%;height:400px"></iframe>
 </div>
 
 </div>
</div> <!-- SECOND DIVISION COMPLETE! -->


<div id="pr" class="w3-container">
<div class="w3-display-container" style="height:400px;">
<div class="w3-half w3-light-blue">
	<iframe src="https://aggieworksdev.tamu.edu/reddy/redyphp/p4.php" style="border:none;width:100%;height:400px"></iframe>
 </div>

 
 </div>
</div> <!-- THIRD DIVISION COMPLETE! -->




</div><!-- MAIN DIVISION COMPLETE! -->
</script>
</body>
</html>
